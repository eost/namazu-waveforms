FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive
ENV DEBIAN_PRIORITY=critical
ENV DEBCONF_NOWARNINGS=yes

RUN apt-get update && apt-get upgrade -y
RUN apt-get update && apt-get -y --no-install-recommends install \
    debhelper \
    devscripts \
    equivs \
    fakeroot \
    gcc \
    help2man \
    lintian \
    locales \
    lsb-release \
    python3 \
    python3-cryptography \
    python3-decorator \
    python3-dev \
    python3-flake8 \
    python3-future \
    python3-gdal \
    python3-geographiclib \
    python3-jsonschema \
    python3-lxml \
    python3-matplotlib \
    python3-mock \
    python3-mpltoolkits.basemap \
    python3-nose \
    python3-numpy \
    python3-pil \
    python3-pip \
    python3-pyproj \
    python3-pyshp \
    python3-requests \
    python3-scipy \
    python3-setuptools \
    python3-sqlalchemy \
    python3-tornado \
    python3-wheel \
    quilt \
    curl \
    jq \
    && pip3 install obspy \
    && rm -rf /var/lib/apt/lists/*
# make sure locale we use in tests is present
RUN locale-gen en_US.UTF-8

COPY bin/waveforms2json.py /usr/local/bin/waveforms2json
COPY bin/waveforms-update.sh /usr/local/bin/waveforms-update
