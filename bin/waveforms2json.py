#!/usr/bin/env python3

# import pprint
import sys
from collections import OrderedDict
from obspy.clients.fdsn.client import Client
from obspy.core.utcdatetime import UTCDateTime
import argparse
import datetime
import json
import requests
import logging


def get_bulk(phases, start_time, end_time):
    return list(map(
        lambda p: [
            p['networkCode'],
            p['stationCode'],
            p['locationCode'] or '',
            p['channelCode'][0:2]+ 'Z'] + [start_time, end_time], phases
    ))

def process_trace(trace, sample_target):
    new_sampling_rate = (sample_target * trace.stats.sampling_rate) / len(trace)
    trace.resample(new_sampling_rate)
    # trace.taper('cosine', max_length=1, side='left')
    # trace.taper(max_percentage=0.5, side='left')
    trace.taper(max_percentage=0.025, side='left')
    try:
        trace.filter("bandpass", freqmin=4, freqmax=20)
    except:
        trace.filter("bandpass", freqmin=0.7, freqmax=2)
    # trace.filter("bandpass", freqmin=8, freqmax=32)
    return trace


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Convert waveforms from an event to a JSON file.')

    parser.add_argument('event_publicid', type=str, help='Event Public ID from Namazu database.')
    parser.add_argument('output_file', type=str, help='JSON output file.')

    parser.add_argument('--sample-target', dest='sample_target', type=int, default=3000, help='Resample trace to match the sample target.')
    parser.add_argument('--namazu', dest='namazu_url', type=str, default='https://api.franceseisme.fr', help='Namazu webservice address.')
    parser.add_argument('--dataselect', dest='dataselect_url', type=str, default='http://ws.resif.fr')

    args = parser.parse_args()

    try:
        # pp = pprint.PrettyPrinter()
        # pp.pprint(args)

        clients = map(lambda u: Client(base_url=u, service_mappings={'event': None, 'station': None}), args.dataselect_url.split(','))

        phases_url = "{}/api/v1/events/{}/phases".format(args.namazu_url, args.event_publicid)
        event_url = "{}/fdsnws/event/1/query?format=json&eventid={}".format(args.namazu_url, args.event_publicid)

        event = requests.get(event_url).json()
        event_properties = event["features"][0]["properties"]
        event_coordinates = event["features"][0]["geometry"]['coordinates']
        print(event_properties)

        data = {
            'event_publicid': args.event_publicid,
            'automatic': event_properties['automatic'],
            'description': event_properties['description'],
            'magnitude': round(event_properties['mag'], 2),
            'magnitude_type': event_properties['magType'],
            'event_type': event_properties['type'],
            'time': event_properties['time'],
            'url': event_properties['url'],
            'longitude': round(event_coordinates[0], 2),
            'latitude': round(event_coordinates[1], 2),
            'depth': round(-event_coordinates[2])
        }

        print(data)

        phases = requests.get(phases_url).json()
        phases = sorted(phases, key = lambda p: p['time'])
        start_time = UTCDateTime(phases[0]['time']) - 10
        end_time = UTCDateTime(phases[-1]['time']) + 30

        data['start_time'] = str(start_time)
        data['end_time'] = str(end_time)

        phases_ordered_dict = OrderedDict(map(lambda p: (
            p['stationCode'],
            {
                'network': p['networkCode'],
                'station': p['stationCode'],
                'location': p['locationCode'] or '',
                'channel': p['channelCode'][0:2]+ 'Z',
                'azimuth': round(p['azimuth']),
                'distance': p['distance'],
                'picks': []
            }),
            phases
        ))

        # # Add picks to entries
        for p in phases:
            phases_ordered_dict[p['stationCode']]['picks'].append({
                'time': p['time'],
                'phase': p['phaseCode']
            })

        bulk = get_bulk(phases, start_time, end_time)

        waveforms = []

        for c in clients:
            try:
                waveforms = c.get_waveforms_bulk(bulk)
                waveforms.trim(start_time, end_time, pad=True, fill_value=0)
                waveforms.merge(method=0, fill_value='interpolate')
                break
            except:
                continue

        for trace in waveforms:
            trace = process_trace(trace, args.sample_target)
            phases_ordered_dict[trace.stats['station']]['sampling_rate'] = trace.stats['sampling_rate']
            phases_ordered_dict[trace.stats['station']]['waveform'] = list(map(int, trace.data))

        data['data'] = list(phases_ordered_dict.values())


        # # for phase_key, phase in phases.items():
        # #     state = "trace" in phase and "OK" or "Missing"
        # #     print("{} => {}".format(phase_key, state))

    except Exception as e:
        logging.error(e)
        data['error'] = str(e)

    finally:
        with open(args.output_file, 'w') as json_file:
            json.dump(data, json_file)

        if 'error' in data:
            sys.exit(1)
